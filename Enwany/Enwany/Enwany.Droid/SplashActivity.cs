using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;

namespace Enwany.Droid
{
    [Activity(Theme = "@style/Theme.Splash", Icon = "@drawable/icon", MainLauncher = true,
        ScreenOrientation = ScreenOrientation.Portrait,
        ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation, NoHistory = true)]
    public class SplashActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_splash);
        }

        protected override void OnResume()
        {
            base.OnResume();

            var startupWork = new Task(() => { Task.Delay(1000); // Simulate a bit of startup work.
            });

            startupWork.ContinueWith(t => { StartActivity(new Intent(Application.Context, typeof(MainActivity))); },
                TaskScheduler.FromCurrentSynchronizationContext());
            startupWork.Start();
        }
    }
}