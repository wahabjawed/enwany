﻿using Android.Widget;
using Xamarin.Forms;
using Toast = Enwany.Droid.Toast;

[assembly: Dependency(typeof(Toast))]

namespace Enwany.Droid
{
    public class Toast : IToast
    {
        public void Show(string message)
        {
            Android.Widget.Toast.MakeText(Forms.Context, message, ToastLength.Long).Show();
        }
    }
}