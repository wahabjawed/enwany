﻿using Android.Preferences;
using Enwany.Droid;
using Xamarin.Forms;

[assembly: Dependency(typeof(SharedPreference))]

namespace Enwany.Droid
{
    public class SharedPreference : ISharedPreference
    {
        public bool GetBoolean(string key)
        {
            var prefs = PreferenceManager.GetDefaultSharedPreferences(Forms.Context);
            return prefs.GetBoolean(key, false);
        }

        public void SetBoolean(string key, bool value)
        {
            var prefs = PreferenceManager.GetDefaultSharedPreferences(Forms.Context);
            var editor = prefs.Edit();
            editor.PutBoolean(key, value);
            // editor.Commit();    // applies changes synchronously on older APIs
            editor.Apply();
        }

        public string GetString(string key)
        {
            var prefs = PreferenceManager.GetDefaultSharedPreferences(Forms.Context);
            return prefs.GetString(key, string.Empty);
        }

        public void SetString(string key, string value)
        {
            var prefs = PreferenceManager.GetDefaultSharedPreferences(Forms.Context);
            var editor = prefs.Edit();
            editor.PutString(key, value);
            // editor.Commit();    // applies changes synchronously on older APIs
            editor.Apply();
        }
    }
}