﻿using AndroidHUD;
using Enwany.Droid;
using Xamarin.Forms;

[assembly: Dependency(typeof(ProgressDialog))]

namespace Enwany.Droid
{
    public class ProgressDialog : IProgressDialog
    {
        public void Show()
        {
            AndHUD.Shared.Show(Forms.Context);
        }

        public void Show(string message)
        {
            AndHUD.Shared.Show(Forms.Context, message);
        }

        public void Dismiss()
        {
            AndHUD.Shared.Dismiss(Forms.Context);
        }
    }
}