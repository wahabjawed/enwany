using System.Globalization;
using Enwany.Droid;
using Java.Util;
using Xamarin.Forms;

[assembly: Dependency(typeof(Localize))]

namespace Enwany.Droid
{
    public class Localize : ILocalize


    {
        public CultureInfo GetCurrentCultureInfo()
        {
            var androidLocale = Locale.Default;
            var netLanguage = androidLocale.ToString().Replace("_", "-"); // turns pt_BR into pt-BR
            return new CultureInfo(netLanguage);
        }
    }
}