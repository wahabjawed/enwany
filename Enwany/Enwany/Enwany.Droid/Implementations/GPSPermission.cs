﻿using Android.Content;
using Android.Provider;
using Enwany.Droid;
using Enwany.Interfaces;
using Xamarin.Forms;

[assembly: Dependency(typeof(GPSPermission))]

namespace Enwany.Droid
{
    public class GPSPermission : IGPSPermission
    {
        public void ShowGPSPrompt()
        {
            var off = Settings.Secure.GetInt(Forms.Context.ContentResolver, Settings.Secure.LocationMode);
            if (off == 0)
            {
                var onGPS = new Intent(Settings.ActionLocationSourceSettings);
                Forms.Context.StartActivity(onGPS);
            }
        }
    }
}