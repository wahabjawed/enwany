﻿using Android.Graphics;
using Android.Views;
using Enwany;
using Enwany.Droid;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using TextAlignment = Xamarin.Forms.TextAlignment;

[assembly: ExportRenderer(typeof(TPButton), typeof(TPButtonRenderer))]

namespace Enwany.Droid
{
    public class TPButtonRenderer : ButtonRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Button> e)
        {
            base.OnElementChanged(e);
            var nativeEntry = Control;
            Control?.SetPadding(Control.PaddingLeft, 0, Control.PaddingRight, 0);

            var button = (TPButton) Element;
            nativeEntry.SetAllCaps(false);
            var style = button.FontStyle;

            var fontFamily = button.FontFamily;


            var font = fontFamily == null
                ? FontUtils.getFontOfType(style)
                : FontUtils.getFontOfTypeAndFamily(style, fontFamily);

            var fontType = Typeface.CreateFromAsset(Forms.Context.Assets, font); // font name specified here
            nativeEntry.Typeface = fontType;

            if (button.Padding != null)
            {
                var padding = (int) button.Padding;
                nativeEntry.SetPadding(padding, padding, padding, padding);
            }

            nativeEntry.Gravity = getGravity(button.TextAlignment);
        }

        private GravityFlags getGravity(TextAlignment alignment)
        {
            switch (alignment)
            {
                case Xamarin.Forms.TextAlignment.Start:
                    return GravityFlags.Left | GravityFlags.CenterVertical;
                case Xamarin.Forms.TextAlignment.End:
                    return GravityFlags.Right | GravityFlags.CenterVertical;
                default:
                    return GravityFlags.CenterHorizontal | GravityFlags.CenterVertical;
            }
        }
    }
}