﻿using Android.Graphics;
using Enwany;
using Enwany.Droid;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(TPLabel), typeof(TPLabelRenderer))]

namespace Enwany.Droid
{
    public class TPLabelRenderer : LabelRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Label> e)
        {
            base.OnElementChanged(e);
            var label = (TPLabel) Element;
            var style = label.FontStyle;

            var fontFamily = label.FontFamily;

            var font = fontFamily == null
                ? FontUtils.getFontOfType(style)
                : FontUtils.getFontOfTypeAndFamily(style, fontFamily);

            if (Control != null)
            {
                Control.Typeface = Typeface.CreateFromAsset(Forms.Context.Assets, font);
            }

/*			if (label.FormattedText != null) {
				SpannableStringBuilder ssb = new SpannableStringBuilder ();

				foreach (Span span in label.FormattedText.Spans) {
					ssb.Append (span.Text);
					ForegroundColorSpan foregroundColorSpan = new ForegroundColorSpan (label.TextColor.ToAndroid ());
					ssb.SetSpan (foregroundColorSpan, ssb.Length () - span.Text.Length, ssb.Length (), SpanTypes.ExclusiveExclusive);
				}

				Control.Typeface = Typeface.CreateFromAsset (Forms.Context.Assets, font);
				Control.TextSize = (float)label.FontSize;
				Control.SetText (ssb, TextView.BufferType.Normal);
			}*/
        }
    }
}