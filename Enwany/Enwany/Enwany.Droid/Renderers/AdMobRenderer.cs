using Android.Gms.Ads;
using Enwany;
using Enwany.Droid;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(AdMobView), typeof(AdMobRenderer))]

namespace Enwany.Droid
{
    public class AdMobRenderer : ViewRenderer<AdMobView, AdView>
    {
        protected override void OnElementChanged(ElementChangedEventArgs<AdMobView> e)
        {
            base.OnElementChanged(e);

            if (Control == null)
            {
                var ad = new AdView(Forms.Context);
                ad.AdSize = AdSize.Banner;
                ad.AdUnitId = Constant.ADMOB_APP_UNIT_IDENTIFIER_ANDROID;
                var requestbuilder = new AdRequest.Builder();

#if DEBUG
                //requestbuilder.AddTestDevice(AdRequest.DeviceIdEmulator);
                requestbuilder.AddTestDevice(Settings.Secure.GetString(Forms.Context.ContentResolver,Settings.Secure.AndroidId));
#endif
                ad.LoadAd(requestbuilder.Build());
                SetNativeControl(ad);
            }
        }
    }
}