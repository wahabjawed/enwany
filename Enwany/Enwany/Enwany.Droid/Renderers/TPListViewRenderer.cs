using Android.OS;
using Enwany.Droid;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(ListView), typeof(TPListViewRenderer))]

namespace Enwany.Droid
{
    public class TPListViewRenderer : ListViewRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<ListView> e)
        {
            base.OnElementChanged(e);
            if (Control == null)
                return;

            var listView = Control;
            listView.DividerHeight = -1;

            if ((int) Build.VERSION.SdkInt >= 21)
            {
                listView.NestedScrollingEnabled = true;
            }
        }
    }
}