﻿using Android.Graphics;
using Android.Widget;
using Enwany;
using Enwany.Droid;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(TPEntry), typeof(TPEntryRenderer))]

namespace Enwany.Droid
{
    public class TPEntryRenderer : EntryRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);
            var nativeEntry = (TextView) Control;

            var Label = (TPEntry) Element;
            var style = Label.FontStyle;

            var font = Typeface.CreateFromAsset(Forms.Context.Assets, FontUtils.getFontOfType(style));
            // font name specified here
            nativeEntry.Typeface = font;
            nativeEntry.Background = null;
            nativeEntry.SetPadding(0, nativeEntry.PaddingTop, 70, nativeEntry.PaddingBottom);
        }
    }
}