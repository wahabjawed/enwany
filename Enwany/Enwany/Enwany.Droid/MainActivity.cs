﻿using Android.App;
using Android.Content.PM;
using Android.Gms.Ads;
using Android.OS;
using FFImageLoading.Forms.Droid;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

namespace Enwany.Droid
{
    [Activity(Label = "Enwany", Icon = "@drawable/icon", Theme = "@style/MainTheme", MainLauncher = false,
        ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : FormsAppCompatActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(bundle);
            Forms.Init(this, bundle);
            MobileAds.Initialize(Forms.Context, Constant.ADMOB_APP_IDENTIFIER_ANDROID);
            CachedImageRenderer.Init();

            LoadApplication(new App());
        }
    }
}