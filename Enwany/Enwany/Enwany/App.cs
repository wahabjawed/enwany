﻿using Xamarin.Forms;

namespace Enwany
{
    public class App : Application
    {
        public NavigationPage appNavigation;

        private IProgressDialog progressDialog;

        public App()
        {
            CurrentApp = this;

			// The root page of your application
			if (DeviceUtils.getBoolean("IsFirstTime"))
			{
				MainPage = new RootPage();

			}
			else { 
				MainPage = new AppIntroView();

			}
        }

        public static App CurrentApp { get; set; }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }

        public void ShowLoader()
        {
            CreateProgressDialogIfNotExists();
            progressDialog.Show();
        }

        public void ShowLoader(string message)
        {
            CreateProgressDialogIfNotExists();
            progressDialog.Show(message);
        }

        public void HideLoader()
        {
            CreateProgressDialogIfNotExists();
            progressDialog.Dismiss();
        }

        private void CreateProgressDialogIfNotExists()
        {
            if (progressDialog == null)
            {
                progressDialog = DependencyService.Get<IProgressDialog>();
            }
        }

        public NavigationPage GetNavigationPage(Page view)
        {
            var navigation = new NavigationPage(view);
            return navigation;
        }
    }
}