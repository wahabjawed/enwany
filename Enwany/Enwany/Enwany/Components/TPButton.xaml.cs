﻿using System;
using System.Runtime.CompilerServices;
using Xamarin.Forms;

namespace Enwany
{
    public partial class TPButton : Button
    {
        public delegate void buttonClickDelegate(object sender, EventArgs e);

        public static readonly BindableProperty FontStyleProperty = BindableProperty.Create("FontStyle",
            typeof(StyleType), typeof(TPButton), StyleType.Regular);

        public static readonly BindableProperty PaddingProperty = BindableProperty.Create("Padding", typeof(int?),
            typeof(TPButton), null);

        public static readonly BindableProperty DisableSourceProperty = BindableProperty.Create("DisableSource",
            typeof(ImageSource), typeof(TPButton), null);

        public static readonly BindableProperty TextAlignmentProperty = BindableProperty.Create("TextAlignment",
            typeof(TextAlignment), typeof(TPButton), TextAlignment.Center);

        private FileImageSource imageSource;

        public TPButton()
        {
            InitializeComponent();
            BackgroundColor = Color.Transparent;
            TextColor = Color.FromHex(ColorUtils.DarkGray);
        }

        public int? Padding
        {
            get { return (int?) GetValue(PaddingProperty); }
            set { SetValue(PaddingProperty, value); }
        }

        public StyleType FontStyle
        {
            get { return (StyleType) GetValue(FontStyleProperty); }
            set { SetValue(FontStyleProperty, value); }
        }

        public ImageSource DisableSource
        {
            get { return (ImageSource) GetValue(DisableSourceProperty); }
            set { SetValue(DisableSourceProperty, value); }
        }

        public TextAlignment TextAlignment
        {
            get { return (TextAlignment) GetValue(TextAlignmentProperty); }
            set { SetValue(TextAlignmentProperty, value); }
        }

        public event buttonClickDelegate nativeButtonClicker;

        protected override void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            base.OnPropertyChanged(propertyName);
            if (propertyName.Equals("IsEnabled") && !IsEnabled && DisableSource != null)
            {
                imageSource = Image;
                Image = (FileImageSource) DisableSource;
            }
            else if (propertyName.Equals("IsEnabled") && IsEnabled && DisableSource != null)
            {
                Image = imageSource;
            }
        }

        public void click()
        {
            if (nativeButtonClicker == null) return;

            var args = new EventArgs();
            nativeButtonClicker(this, args);
        }
    }
}