﻿using System;
using System.Windows.Input;
using Xamarin.Forms;

namespace Enwany
{
    public partial class TPListView : ListView
    {
        public static readonly BindableProperty TotalCountProperty = BindableProperty.Create("TotalCount", typeof(int),
            typeof(TPListView), 0, BindingMode.OneWay);

        public static readonly BindableProperty CountProperty = BindableProperty.Create("Count", typeof(int),
            typeof(TPListView), 0, BindingMode.TwoWay);

        public static readonly BindableProperty CurrentPageProperty = BindableProperty.Create("CurrentPage", typeof(int),
            typeof(TPListView), 1, BindingMode.TwoWay);

        public static readonly BindableProperty PageCommandProperty = BindableProperty.Create("PageCommand",
            typeof(ICommand), typeof(TPListView), null, BindingMode.TwoWay);

        public static readonly BindableProperty LoadingProperty = BindableProperty.Create("Loading", typeof(bool),
            typeof(TPListView), false, BindingMode.TwoWay, propertyChanged: onLoadingChanged);

        public TPListView() : base(ListViewCachingStrategy.RecycleElement)
        {
            InitializeComponent();
            Refreshing += onRefresh;
        }

        public int CurrentPage
        {
            get { return (int) GetValue(CurrentPageProperty); }
            set { SetValue(CurrentPageProperty, value); }
        }

        public int TotalCount
        {
            get { return (int) GetValue(TotalCountProperty); }
            set { SetValue(TotalCountProperty, value); }
        }

        public int Count
        {
            get { return (int) GetValue(CountProperty); }
            set { SetValue(CountProperty, value); }
        }

        public ICommand PageCommand
        {
            get { return (ICommand) GetValue(PageCommandProperty); }
            set { SetValue(PageCommandProperty, value); }
        }

        public bool Loading
        {
            get { return (bool) GetValue(LoadingProperty); }
            set { SetValue(LoadingProperty, value); }
        }

        private void onRefresh(object sender, EventArgs e)
        {
            CurrentPage = 1;
        }

        protected override void SetupContent(Cell content, int index)
        {
            base.SetupContent(content, index);

            if (TotalCount > Count && Count - 1 == index)
            {
                var nextPage = CurrentPage + 1;

                if (PageCommand.CanExecute(nextPage) && CurrentPage < nextPage && !Loading)
                {
                    PageCommand.Execute(nextPage);
                }
            }
        }

        private static void onLoadingChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var list = bindable as TPListView;
            if (list == null)
                return;

            var view = (StackLayout) list.Footer;
            view.HeightRequest = (bool) newValue ? 40 : 0;
        }
    }
}