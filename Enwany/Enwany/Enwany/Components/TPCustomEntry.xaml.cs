﻿using System;
using System.Windows.Input;
using PropertyChanged;
using Xamarin.Forms;

namespace Enwany
{
    [ImplementPropertyChanged]
    public partial class TPCustomEntry : StackLayout
    {
        public static readonly BindableProperty FontStyleProperty = BindableProperty.Create("FontStyle",
            typeof(StyleType), typeof(TPEntry), StyleType.Regular);

        public static readonly BindableProperty PlaceholderProperty = BindableProperty.Create("Placeholder",
            typeof(string), typeof(string), "");

        public static readonly BindableProperty MaxLengthProperty = BindableProperty.Create("MaxLength", typeof(int),
            typeof(TPCustomEntry), int.MaxValue);

        public static readonly BindableProperty TextProperty = BindableProperty.Create("Text", typeof(string),
            typeof(TPCustomEntry), "", BindingMode.TwoWay, null, handleTextChanged);

        public static readonly BindableProperty FontSizeProperty = BindableProperty.Create("FontSize", typeof(int),
            typeof(TPLabel), 16);

        public static readonly BindableProperty IconProperty = BindableProperty.Create("Icon", typeof(string),
            typeof(string), "");

        public static readonly BindableProperty LabelTextProperty = BindableProperty.Create("Text", typeof(string),
            typeof(TPLabel), "");

        public static readonly BindableProperty LabelColorProperty = BindableProperty.Create("TextColor", typeof(Color),
            typeof(TPLabel), Color.Transparent);

        public static readonly BindableProperty IsPasswordProperty = BindableProperty.Create("IsPassword", typeof(bool),
            typeof(TPEntry), false);

        public static readonly BindableProperty IsEnableProperty = BindableProperty.Create("IsEnable", typeof(bool),
            typeof(bool), true);

        public static readonly BindableProperty UnderlineErrorMessageProperty =
            BindableProperty.Create("UnderlineErrorMessage", typeof(Color), typeof(BoxView),
                ColorUtils.ToColor(ColorUtils.GrayUnderline));

        public static readonly BindableProperty ErrorMessageTextColorProperty =
            BindableProperty.Create("ErrorMessageTextColor", typeof(Color), typeof(TPLabel), Color.Red);

        public static readonly BindableProperty KeyboardProperty = BindableProperty.Create("Keyboard", typeof(Keyboard),
            typeof(TPEntry), Keyboard.Chat);

        public static readonly BindableProperty PlaceHolderFontSizeProperty =
            BindableProperty.Create("PlaceHolderTextSize", typeof(int), typeof(TPLabel), 12);

        public static readonly BindableProperty AutoClearOnIconClickProperty =
            BindableProperty.Create("ShouldClearOnImageClick", typeof(bool), typeof(bool), false);

        public static readonly BindableProperty IconAlignmentProperty = BindableProperty.Create("IconAlignment",
            typeof(LayoutOptions), typeof(TPCustomEntry), LayoutOptions.End,
            propertyChanged: iconAlignmentPropertyChanged);

        public TPCustomEntry()
        {
            InitializeComponent();
            root.BindingContext = this;
            PlaceholderColor = ColorUtils.ToColor(ColorUtils.Gray);
            ClearFieldValue = new Command(() =>
            {
                if (ShouldClearOnImageClick && Text.Length > 0)
                {
                    IsFieldEmpty = true;
                    Text = string.Empty;
                    ClearValue(TextProperty);
                }
            });
        }

        public TPEntry Entry
        {
            get { return Feild; }
        }

        public int FontSize
        {
            get { return (int) GetValue(FontSizeProperty); }
            set { SetValue(FontSizeProperty, value); }
        }

        public int MaxLength
        {
            get { return (int) GetValue(MaxLengthProperty); }
            set { SetValue(MaxLengthProperty, value); }
        }

        public LayoutOptions IconAlignment
        {
            get { return (LayoutOptions) GetValue(IconAlignmentProperty); }
            set { SetValue(IconAlignmentProperty, value); }
        }

        public ICustomEntry Listener { get; set; }

        public ICommand ClearFieldValue { get; set; }

        public bool ShouldClearOnImageClick
        {
            get { return (bool) GetValue(AutoClearOnIconClickProperty); }
            set { SetValue(AutoClearOnIconClickProperty, value); }
        }

        public int PlaceHolderTextSize
        {
            get { return (int) GetValue(PlaceHolderFontSizeProperty); }
            set { SetValue(PlaceHolderFontSizeProperty, value); }
        }

        public Color PlaceholderColor { get; set; }

        public Keyboard Keyboard
        {
            get { return (Keyboard) GetValue(KeyboardProperty); }
            set { SetValue(KeyboardProperty, value); }
        }

        public Color ErrorMessageTextColor
        {
            get { return (Color) GetValue(ErrorMessageTextColorProperty); }
            set { SetValue(ErrorMessageTextColorProperty, value); }
        }

        public Color UnderlineErrorMessage
        {
            get { return (Color) GetValue(UnderlineErrorMessageProperty); }
            set { SetValue(UnderlineErrorMessageProperty, value); }
        }

        public bool IsPassword
        {
            get { return (bool) GetValue(IsPasswordProperty); }
            set { SetValue(IsPasswordProperty, value); }
        }

        public bool IsEnable
        {
            get { return (bool) GetValue(IsEnableProperty); }
            set { SetValue(IsEnableProperty, value); }
        }

        public Color LabelColor
        {
            get { return (Color) GetValue(LabelColorProperty); }
            set { SetValue(LabelColorProperty, value); }
        }

        public string LabelText
        {
            get { return (string) GetValue(LabelTextProperty); }
            set { SetValue(LabelTextProperty, value); }
        }

        public StyleType FontStyle
        {
            get { return (StyleType) GetValue(FontStyleProperty); }
            set { SetValue(FontStyleProperty, value); }
        }

        public string Placeholder
        {
            get { return (string) GetValue(PlaceholderProperty); }
            set { SetValue(PlaceholderProperty, value); }
        }

        public string Text
        {
            get { return (string) GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        public string Icon
        {
            get { return (string) GetValue(IconProperty); }
            set { SetValue(IconProperty, value); }
        }

        public bool IsError
        {
            get { return !string.IsNullOrEmpty(LabelText); }
            set
            {
                if (value)
                {
                    LabelColor = IsFieldEmpty ? Color.Transparent : ColorUtils.ToColor(Theme.Red);
                    UnderlineErrorMessage = ColorUtils.ToColor(Theme.Red);
                }
                else
                {
                    UnderlineErrorMessage = ColorUtils.ToColor(ColorUtils.GrayUnderline);
                    LabelText = string.Empty;
                }
            }
        }

        public bool IsFieldEmpty { get; set; }

        private static void handleTextChanged(BindableObject bindable, object oldValue, object newValue)
        {
            if (newValue == null)
                newValue = "";

            if (newValue.ToString().Length <= ((TPCustomEntry) bindable).MaxLength)
                //If it is more than your character restriction
            {
                ((TPCustomEntry) bindable).Text = newValue.ToString();
            }
            else
            {
                ((TPCustomEntry) bindable).Text = oldValue.ToString();
            }

            ((TPCustomEntry) bindable).OnChange();
        }

        private void OnChange()
        {
            if (!string.IsNullOrEmpty(Text))
            {
                if (Feild.IsFocused)
                {
                    LabelColor = ColorUtils.ToColor(Theme.SecondaryColorLight);
                    UnderlineErrorMessage = ColorUtils.ToColor(Theme.SecondaryColorLight);
                }
                else
                    LabelColor = ColorUtils.ToColor(ColorUtils.Gray);
            }
            else
            {
                LabelColor = Color.Transparent;
                UnderlineErrorMessage = ColorUtils.ToColor(ColorUtils.GrayUnderline);
            }
            LabelText = string.Empty;
        }

        private static void iconAlignmentPropertyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var customEntry = bindable as TPCustomEntry;
            if (customEntry == null)
                return;

            customEntry.IconImg.HorizontalOptions = (LayoutOptions) newValue;
            if (newValue.Equals(LayoutOptions.Start))
            {
                customEntry.EntryContiner.Padding = new Thickness(30, 0, 0, 0);
            }
        }

        private void OnFieldFocused(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Feild.Text))
            {
                LabelColor = ColorUtils.ToColor(Theme.SecondaryColorLight);
                UnderlineErrorMessage = ColorUtils.ToColor(Theme.SecondaryColorLight);
            }
        }

        private void OnFieldUnfocused(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Feild.Text))
                LabelColor = ColorUtils.ToColor(ColorUtils.Gray);


            UnderlineErrorMessage = ColorUtils.ToColor(ColorUtils.GrayUnderline);
            ((TPEntry) sender).Unfocus();
        }

        private void OnComplete(object sender, EventArgs e)
        {
            if (Listener != null)
                Listener.OnCustomEntryEditingFinish((TPEntry) sender);
        }

        private void OnTextChagned(object sender, EventArgs e)
        {
            IsError = false;
            if (Listener != null)
                Listener.OnCustomEntryTextChanged((TPEntry) sender);
        }

        public void SetFocus()
        {
            Feild.Focus();
        }

        public void Clear()
        {
            Feild.Text = "";
            IsFieldEmpty = true;
            Text = string.Empty;
            ClearValue(TextProperty);
        }

        public void HideErrorMessage()
        {
            UnderlineErrorMessage = ColorUtils.ToColor(ColorUtils.GrayUnderline);
            LabelText = string.Empty;
        }
    }
}