﻿namespace Enwany
{
    public partial class TPProfileImage : TPButton
    {
        public TPProfileImage()
        {
            InitializeComponent();
            BindingContext = this;
        }
    }
}