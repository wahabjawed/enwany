﻿using Xamarin.Forms;

namespace Enwany
{
    public partial class TPEntry : Entry
    {
        public static readonly BindableProperty FontStyleProperty = BindableProperty.Create("FontStyle",
            typeof(StyleType), typeof(TPEntry), StyleType.Regular);

        public TPEntry()
        {
            InitializeComponent();
            TextColor = ColorUtils.ToColor(ColorUtils.DarkGray);
            PlaceholderColor = ColorUtils.ToColor(ColorUtils.Gray);
        }

        public StyleType FontStyle
        {
            get { return (StyleType) GetValue(FontStyleProperty); }
            set { SetValue(FontStyleProperty, value); }
        }
    }
}