﻿using Xamarin.Forms;

namespace Enwany
{
    public partial class TPLabel : Label
    {
        public static readonly BindableProperty FontStyleProperty = BindableProperty.Create("FontStyle",
            typeof(StyleType), typeof(TPLabel), StyleType.Regular);

        public TPLabel()
        {
            InitializeComponent();
            TextColor = Color.FromHex(ColorUtils.DarkGray);
            FontSize = 14;
        }

        public StyleType FontStyle
        {
            get { return (StyleType) GetValue(FontStyleProperty); }
            set { SetValue(FontStyleProperty, value); }
        }
    }
}