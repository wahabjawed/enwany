﻿using Xamarin.Forms;

namespace Enwany
{
    public interface IPageWrapper
    {
        Page getDetailPage();

        NavigationPage getNavigationPage(Page detailpage);

        bool shouldShowHome();
    }
}