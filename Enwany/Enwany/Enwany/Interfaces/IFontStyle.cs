﻿namespace Enwany
{
    public interface IFontStyle
    {
        StyleType FontStyle();
    }
}