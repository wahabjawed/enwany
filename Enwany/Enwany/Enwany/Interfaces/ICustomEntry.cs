﻿namespace Enwany
{
    public interface ICustomEntry
    {
        void OnCustomEntryEditingFinish(TPEntry entry);
        void OnCustomEntryTextChanged(TPEntry entry);
    }
}