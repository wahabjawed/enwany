﻿namespace Enwany.Interfaces
{
    public interface IGPSPermission
    {
        void ShowGPSPrompt();
    }
}