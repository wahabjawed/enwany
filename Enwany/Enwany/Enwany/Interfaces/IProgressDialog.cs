﻿namespace Enwany
{
    public interface IProgressDialog
    {
        void Show();

        void Show(string message);

        void Dismiss();
    }
}