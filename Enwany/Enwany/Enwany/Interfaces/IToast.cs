﻿namespace Enwany
{
    public interface IToast
    {
        void Show(string message);
    }
}