﻿namespace Enwany
{
    public interface ISharedPreference
    {
        bool GetBoolean(string key);

        void SetBoolean(string key, bool value);

        void SetString(string key, string value);

        string GetString(string key);
    }
}