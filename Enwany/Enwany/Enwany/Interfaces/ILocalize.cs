﻿using System.Globalization;

namespace Enwany
{
    public interface ILocalize
    {
        CultureInfo GetCurrentCultureInfo();
    }
}