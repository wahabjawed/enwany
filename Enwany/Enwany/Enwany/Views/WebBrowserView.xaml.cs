﻿using System.Threading.Tasks;

namespace Enwany
{
    public partial class WebBrowserView : BaseView
    {
        public WebBrowserView()
        {
        }

        public WebBrowserView(string URL, string _Title)
        {
            InitializeComponent();
            Title = _Title;
            Task.Delay(1000);
            webView.Source = URL;
        }
    }
}