﻿using System;
using System.Collections.ObjectModel;
using Xamarin.Forms;

namespace Enwany
{
    public partial class AppIntroView : BaseView
    {
        AppIntroViewViewModel viewModel = new AppIntroViewViewModel();
        private int pos = 0;

        public AppIntroView()
        {
            InitializeComponent();
            BindingContext = viewModel;
            ShowNavigationBar(false);
        }

        private void OnNextButtonTapped(object sender, EventArgs e)
        {
            if (pos < 7)
            {
                CarouselView.Position = pos = pos + 1;
                if (pos == 7)
                {
                    (sender as Image).IsVisible = false;
                    skipLabel.Text = "Continue".ToUpperInvariant();
                }
            }
        }

        private void OnSkipButtonTapped(object sender, EventArgs e)
        {
			DeviceUtils.setBoolean("IsFirstTime", true);
            App.CurrentApp.MainPage = new RootPage();
        }

        private void CarouselView_OnPositionSelected(object sender, SelectedPositionChangedEventArgs e)
        {
            pos = (int) e.SelectedPosition;

            if (pos < 7)
            {
                NextImage.IsVisible = true;
                skipLabel.Text = "skip".ToUpperInvariant();
            }
            else
            {
                NextImage.IsVisible = false;
                skipLabel.Text = "Continue".ToUpperInvariant();
            }
        }
    }

    public class AppIntroViewViewModel
    {
        public ObservableCollection<AppIntro> Pages { get; set; }

        public AppIntroViewViewModel()
        {
            Pages = new ObservableCollection<AppIntro>
            {
                new AppIntro()
                {
                    WalkthroughImage = "t1"
                },

                new AppIntro()
                {
                    WalkthroughImage = "t2"
                },

                new AppIntro()
                {
                    WalkthroughImage = "t3"
                },

                new AppIntro()
                {
                    WalkthroughImage = "t4"
                },

                new AppIntro()
                {
                    WalkthroughImage = "t5"
                },

                new AppIntro()
                {
                    WalkthroughImage = "t6"
                },

                new AppIntro()
                {
                    WalkthroughImage = "t7"
                },

                new AppIntro()
                {
                    WalkthroughImage = "t8"
                }
            };
        }
    }

    public class AppIntro
    {
        public string WalkthroughImage { get; set; }
    }
}
