﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Reflection;
using Enwany.Interfaces;
using Enwany.Resources;
using Plugin.Geolocator;
using Plugin.Share;
using PropertyChanged;
using RestSharp.Portable;
using RestSharp.Portable.HttpClient;
using Xamarin.Forms;
using Xamarin.Forms.GoogleMaps;

namespace Enwany
{
    [ImplementPropertyChanged]
    public partial class HomeView : BaseView
    {
        private readonly Assembly assembly;
        private double lat, longt;
        private string mapCodes;

        public HomeView()
        {
            InitializeComponent();
            Title = AppResources.Home;
            searchFor.Placeholder = AppResources.search;
            assembly = typeof(HomeView).GetTypeInfo().Assembly;
			Device.OnPlatform(Android: () => { direction.IsVisible=false; });

		}

        private async void GetCurrentLatLong()
        {
            try
            {
                var results = await CrossGeolocator.Current.GetPositionAsync(5000);
                CreatePin(results.Latitude, results.Longitude);
            }
            catch (Exception ex)
            {
                var result =
                    await
                        DisplayAlert(AppResources.GPS_TITLE, AppResources.GPS_TEXT, AppResources.OK, AppResources.CANCEL);
                if (result)
                {
                    Device.OnPlatform(Android: () => { DependencyService.Get<IGPSPermission>().ShowGPSPrompt(); });
                }

                //Set map cordinates to IRAQ
                CreatePin(33.281224, 44.377651);
            }
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            GetCurrentLatLong();
        }

        private void OnMapClicked(object sender, MapClickedEventArgs e)
        {
            CreatePin(e.Point.Latitude, e.Point.Longitude);
        }

        private void CreatePin(double latitude, double longitude)
        {
			lat = latitude;
			longt = longitude;
            map.Pins.Clear();
            var stream = assembly.GetManifestResourceStream($"Enwany.app_icon.png");


			Device.OnPlatform(iOS: () => { stream = assembly.GetManifestResourceStream($"Enwany.app_icon_ios.png"); });

			BitmapDescriptor bitmap = BitmapDescriptorFactory.FromStream(stream);


            var p = new Pin
            {
                Type = PinType.Place,
                Label = "عنواني",
                IsDraggable = true,
                Icon = bitmap,

                Position = new Position(latitude, longitude)
            };


            map.Pins.Add(p);
            map.SelectedPin = p;

            map.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(latitude, longitude), Distance.FromMeters(5000)));


            RestAPICallEncode(latitude, longitude);
        }

        private void OnMapPinDragEndEvent(object sender, PinDragEventArgs e)
        {
            CreatePin(e.Pin.Position.Latitude, e.Pin.Position.Longitude);
        }

        private async void RestAPICallEncode(double latitude, double longitude)
        {
            try
            {
                var URL = string.Format("{0}{1}", "https://",
                    $"api.mapcode.com/mapcode/codes/{latitude},{longitude}?territory=IRQ&alphabet=roman&include=territory,alphabet");

                using (var client = new RestClient(new Uri(URL)))
                {
					
                    var request = new RestRequest(Method.GET);
                    var result = await client.Execute<MapCodeData>(request);
                    mapCodes = result.Data.mapcodes[0].mapcode;
                    Title = result.Data.mapcodes[0].mapcode;
                }
            }
            catch (Exception ex)
            {
                ShowToast("The Map Code was not found.");
            }
        }

        private async void RestAPICallDecode(string mapCode)
        {
            try
            {
                var URL = string.Format("{0}{1}", "https://", $"api.mapcode.com/mapcode/coords/{mapCode}?context=IRQ");

                using (var client = new RestClient(new Uri(URL)))
                {
                    var request = new RestRequest(Method.GET);
                    var result = await client.Execute<MapCordinatesData>(request);
                    lat = result.Data.latDeg;
                    longt = result.Data.lonDeg;

                    CreatePin(lat, longt);
                }
            }
            catch (Exception ex)
            {
                ShowToast("Invalid Map Code");
            }
        }

        private void OnSearchButtonPressed(object sender, EventArgs e)
        {
            var s = sender as SearchBar;
            if (s != null)
            {
                RestAPICallDecode(s.Text);
            }
        }

		private void OnDirectionButtonPressed(object sender, EventArgs e)
		{
			String address = lat+","+longt;

			Device.OpenUri(
   new Uri(string.Format("https://www.google.com/maps?saddr=My+Location&daddr={0}", WebUtility.UrlEncode(address))));

		}

        private void OnShareMenuItemClicked(object sender, EventArgs e)
        {
            CrossShare.Current.Share(
                $"{mapCodes} {AppResources.SHARE_TEXT_01} \n {AppResources.SHARE_TEXT_02} \n www.enwany.com",
                "Share Map Code");
        }

        private void OnLocationMenuItemClicked(object sender, EventArgs e)
        {
            GetCurrentLatLong();
        }
    }

    public class MapCordinatesData
    {
        public double latDeg { get; set; }
        public double lonDeg { get; set; }
    }

    public class MapCodeData
    {
        public MapCodeResult international;
        public MapCodeResult local;

        public List<MapCodeResult> mapcodes;

        public class MapCodeResult
        {
            public string mapcode { get; set; }
            public string mapcodeInAlphabet { get; set; }
            public string territory { get; set; }
            public string territoryInAlphabet { get; set; }
        }
    }
}