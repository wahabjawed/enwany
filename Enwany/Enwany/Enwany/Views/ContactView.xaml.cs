﻿using Enwany.Resources;

namespace Enwany
{
    public partial class ContactView : BaseView
    {
        public ContactView()
        {
            InitializeComponent();
            Title = AppResources.Contact;
        }
    }
}