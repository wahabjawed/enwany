﻿using System;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using Enwany.Resources;

namespace Enwany
{
    public static class StringExtension
    {
        /// <summary>
        ///     03/15/2016
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string ToMMDDYYYYDateFormat(this string input)
        {
            var mmddyyyy = string.Empty;
            DateTime dateTime;
            mmddyyyy = DateTime.TryParse(input, out dateTime) ? string.Format("{0:MM/dd/yyyy}", dateTime) : string.Empty;
            return mmddyyyy;
        }

        /// <summary>
        ///     3/9/2016
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string ToMDDYYYYDateFormat(this string input)
        {
            var mmddyyyy = string.Empty;
            DateTime dateTime;
            mmddyyyy = DateTime.TryParse(input, out dateTime) ? string.Format("{0:M/d/yyy}", dateTime) : string.Empty;
            return mmddyyyy;
        }

        public static string ToCurrencySymbol(this string input)
        {
            var currencyValue = string.Empty;
            var culture = CultureInfo.CurrentCulture;
            if (!string.IsNullOrEmpty(input))
            {
                currencyValue = string.Concat(culture.NumberFormat.CurrencySymbol, input);
            }

            return currencyValue;
        }

        /// <summary>
        ///     "Jan 04, 2016, 1:44 AM"
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string ToLongDateTimeFormat(this string input)
        {
            var mmddyyyy = string.Empty;
            DateTime dateTime;
            mmddyyyy = DateTime.TryParse(input, out dateTime)
                ? string.Format("{0:MMM dd, yyyy, h:mm tt}", dateTime)
                : string.Empty;
            return mmddyyyy.ToUpper();
        }

        /// <summary>
        ///     Convert UTC Time to Local Time , expects only Time in UTC as input
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string ToLocalTime(this string input)
        {
            var tzi = TimeZoneInfo.Local;
            DateTime localDate;
            if (DateTime.TryParseExact(input.Substring(0, input.Length - 1), "HHmm", CultureInfo.InvariantCulture,
                DateTimeStyles.None, out localDate))
            {
                localDate = localDate.AddHours(tzi.BaseUtcOffset.Hours);
                localDate = localDate.AddMinutes(tzi.BaseUtcOffset.Minutes);
                return (localDate.ToString("hh:mm tt") + " " + tzi.StandardName).ToUpper();
            }
            return string.Empty;
        }

        /// <summary>
        ///     Convert DateTime to Local Date Time from UTC
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static DateTime ToLocalDateTime(this string input)
        {
            var utcTime = DateTime.Parse(input);
            var runtimeUtc = DateTime.SpecifyKind(utcTime, DateTimeKind.Utc);
            var localVersion = runtimeUtc.ToLocalTime();

            return localVersion;
        }

        public static string ToPhoneNumber(this string value)
        {
            value = KeepOnlyNumbers(value);
            value = new Regex(@"\D")
                .Replace(value, string.Empty);
            value = value.TrimStart('1');
            if (value.Length == 7)
                return Convert.ToInt64(value).ToString("###-####");
            if (value.Length == 10)
                return Convert.ToInt64(value).ToString("(###) ###-####");
            if (value.Length > 10)
                return Convert.ToInt64(value)
                    .ToString("###-###-#### " + new string('#', value.Length - 10));
            return value;
        }

        public static string KeepOnlyNumbers(this string input)
        {
            return Regex.Replace(input, "[^0-9_.]+", "", RegexOptions.None);
        }

        public static string ToMMDDYYYYatHMMTTDateTimeFormat(this string input)
        {
            var mmddyyyy = string.Empty;
            var hmmtt = string.Empty;
            DateTime dateTime;
            mmddyyyy = DateTime.TryParse(input, out dateTime)
                ? string.Format("{0:MM/dd/yyyy} {1} ", dateTime, AppResources.AT)
                : string.Empty;
            hmmtt = DateTime.TryParse(input, out dateTime) ? string.Format("{0:h:mm tt}", dateTime) : string.Empty;
            return mmddyyyy + hmmtt.ToUpper();
        }

        public static string ToHMMTTTimeFormat(this string input)
        {
            var hmmtt = string.Empty;
            DateTime dateTime;
            hmmtt = DateTime.TryParse(input, out dateTime) ? string.Format("{0:h:mm tt}", dateTime) : string.Empty;
            return hmmtt.ToUpper();
        }

        public static string ToProperCase(this string input)
        {
            // If there are 0 or 1 characters, just return the string.
            if (string.IsNullOrEmpty(input) || string.IsNullOrWhiteSpace(input)) return input;
            if (input.Length < 2) return input.ToUpper();

            // Start with the first character.
            var result = input.Substring(0, 1).ToUpper();

            // Add the remaining characters.
            for (var i = 1; i < input.Length; i++)
            {
                if (char.IsUpper(input[i])) result += " ";
                result += input[i];
            }

            return result;
        }


        public static string RemoveMiles(this string input)
        {
            return input.Replace(AppResources.DISTANCE_UNIT, "").Trim();
        }

        public static string ToTitleCase(this string thestring)
        {
            return string.Join(" ",
                thestring.Split(' ').Select(i => i.Substring(0, 1).ToUpper() + i.Substring(1).ToLower()).ToArray());
        }

        public static string Ellipsize(this string thestring, int length)
        {
            if (length > 0 && thestring.Length > length)
            {
                return thestring.Substring(0, length) + "...";
            }
            return thestring;
        }

        public static string ToValueUnit(this string value, string unit)
        {
            if (string.IsNullOrEmpty(value))
            {
                return "";
            }

            return string.Format("{0} {1}", value, unit);
        }


        public static string ToFormattedValue(this string value, string format)
        {
            if (string.IsNullOrEmpty(value))
            {
                return "";
            }
            return string.Format(format, value);
        }

        public static string ToLocalDateTimeString(this string value)
        {
            DateTime parsedDateTime;
            if (DateTime.TryParse(value, out parsedDateTime) && DateTime.MinValue != parsedDateTime)
            {
                var runtimeUtc = DateTime.SpecifyKind(parsedDateTime, DateTimeKind.Utc);
                var localVersion = runtimeUtc.ToLocalTime();
                return localVersion.ToString();
            }
            return string.Empty;
        }

        public static bool NotEquals(this string value, string otherValue)
        {
            return value != null && otherValue != null && !value.Equals(otherValue);
        }
    }
}