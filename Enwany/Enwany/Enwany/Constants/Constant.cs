﻿namespace Enwany
{
    public static class Constant
    {
        #region Error Code

        public const string NOT_EMPTY_ERROR_CODE = "notempty_error";

        #endregion

        public const string PhoneNumberRegex = "(\\d-)?(\\d{3}-)?\\d{3}-\\d{4}";

        public const string EmailAddressRegex =
            @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
            @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$";

        public const string PasswordRegex = "(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])";

        public const string Text = "Text";

        public const string NotApplicable = "N/A";

        public const string TranslateExceptionFormatText =
            "Key '{0}' was not found in resources '{1}' for culture '{2}'.";

        public const string ENWANY_API_VERSION = "1";

        public const string AppName = "Enwany";

        public const int ERROR_CODE_NO_INTERNET = 599;

        public const string IS_COLORBLIND = "color_blind";

        public const string PLATFORM_ANDROID = "ANDROID";

        public const string PLATFORM_IOS = "IOS";

        #region APP IDENTIFIERS

        public const string ADMOB_APP_UNIT_IDENTIFIER_ANDROID = "ca-app-pub-2447389984898739/4212416806";

        public const string ADMOB_APP_IDENTIFIER_ANDROID = "ca-app-pub-2447389984898739~2735683604";

        public const string MOCK_ADMOB_APP_IDENTIFIER = "ca-app-pub-3940256099942544/6300978111";

        #endregion
    }
}