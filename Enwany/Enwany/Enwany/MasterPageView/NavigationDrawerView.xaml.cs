﻿using System.Collections.Generic;
using PropertyChanged;
using Xamarin.Forms;

namespace Enwany
{
    [ImplementPropertyChanged]
    public partial class NavigationDrawerView : ContentPage
    {
        public NavigationDrawerView()
        {
            InitializeComponent();

            Icon = "icon";
            Title = "menu"; // The Title property must be set.
            BackgroundColor = Color.FromHex(ColorUtils.AppBackgroundColor);

            List<MenuItem> data = new MenuListData();
            drawer.ItemsSource = data;
            //drawer.SelectedItem = data[0];
        }

        public ListView drawerView
        {
            get { return drawer; }
        }
    }
}