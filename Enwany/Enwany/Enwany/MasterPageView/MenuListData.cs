using System.Collections.Generic;
using Enwany.Resources;

namespace Enwany
{
    public class MenuListData : List<MenuItem>
    {
        public MenuListData()
        {
            Add(new MenuItem
            {
                Title = AppResources.Home,
                IconSource = "icon_home.png",
                TargetType = typeof(HomeView)
            });
            Add(new MenuItem
            {
                Title = AppResources.FAQ,
                IconSource = "icon_faq",
                TargetType = typeof(WebBrowserView)
            });

            Add(new MenuItem
            {
                Title = AppResources.About,
                IconSource = "icon_about",
                TargetType = typeof(WebBrowserView)
            });

            Add(new MenuItem
            {
                Title = AppResources.Contact,
                IconSource = "icon_phone",
                TargetType = typeof(ContactView)
            });

            Add(new MenuItem
            {
                Title = AppResources.Donate,
                IconSource = "icon_donate",
                TargetType = typeof(WebBrowserView)
            });
        }
    }
}