﻿using System;
using System.Threading.Tasks;
using Enwany.Resources;
using Xamarin.Forms;

namespace Enwany
{
    public class RootPage : MasterDetailPage
    {
        private MenuItem lastSelectedItem;
        private readonly NavigationDrawerView menuPage;
        private WebBrowserView web;

        public RootPage()
        {
            menuPage = new NavigationDrawerView();
            menuPage.Icon = "menu_icon";

            menuPage.drawerView.ItemSelected += (sender, e) =>
            {
                iOSTapEffect(sender, e);
                NavigateTo(sender, e.SelectedItem as MenuItem);
            };
            menuPage.drawerView.ItemTapped += (sender, e) => Close();
            Master = menuPage;
            Detail = new NavigationPage(new HomeView());
        }

        private void Close()
        {
            IsPresented = false;
        }

        private async void iOSTapEffect(object sender, SelectedItemChangedEventArgs e)
        {
            var menu = (MenuItem) e.SelectedItem;
            if (menu == null)
                return;

            menu.IsSelected = true;
            await Task.Delay(500);
            menu.IsSelected = false;
        }

        private async void NavigateTo(object sender, MenuItem menu)
        {
            if (((View) sender).InputTransparent || menu == null || menu == lastSelectedItem)
                return;

            var displayPage = (Page) Activator.CreateInstance(menu.TargetType);

            menuPage.drawerView.SelectedItem = null;

            if (menu.Title.Equals(AppResources.About))
            {
                web = new WebBrowserView("http://www.enwany.com/aboutus", AppResources.About);
            }
            else if (menu.Title.Equals(AppResources.FAQ))
            {
                web = new WebBrowserView("http://www.enwany.com/faq", AppResources.FAQ);
            }
            else if (menu.Title.Equals(AppResources.Donate))
            {
                web = new WebBrowserView("http://www.enwany.com/supportus/", AppResources.Donate);
            }
			else if (menu.Title.Equals(AppResources.Contact))
			{
				web = new WebBrowserView("http://www.enwany.com/the-team", AppResources.Contact);
			}
            else if (menu.Title.Equals(AppResources.Home))
            {
                Detail = new NavigationPage(displayPage);
                IsPresented = false;
                await Task.Delay(500);
                ((View) sender).InputTransparent = false;
                return;
            }

            Detail = new NavigationPage(web);
            IsPresented = false;

            await Task.Delay(500);
            ((View) sender).InputTransparent = false;
        }

        private void resetSelection()
        {
            if (lastSelectedItem != null)
                lastSelectedItem.IsSelected = false;

            lastSelectedItem = null;
        }

        private async void updateItems(MenuItem selectedItem)
        {
            if (selectedItem == lastSelectedItem)
            {
                IsPresented = false;
                return;
            }

            await Task.Delay(500);

            if (lastSelectedItem != null)
                lastSelectedItem.IsSelected = false;

            lastSelectedItem = selectedItem;
        }

        protected override bool OnBackButtonPressed()
        {
            resetSelection();
            return base.OnBackButtonPressed();
        }
    }
}