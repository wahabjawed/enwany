﻿using System;
using PropertyChanged;
using Xamarin.Forms;

namespace Enwany
{
    [ImplementPropertyChanged]
    public class MenuItem
    {
        private bool isSelected;

        public MenuItem()
        {
            IsSelected = false;
            SetSelectedState();
        }

        public string Title { get; set; }

        public string IconSource { get; set; }

        public Type TargetType { get; set; }

        public bool IsSelected
        {
            get { return isSelected; }
            set
            {
                isSelected = value;
                SetSelectedState();
            }
        }


        public Color BackgroundColor { get; set; }

        public double Opacity { get; set; }

        private void SetSelectedState()
        {
            if (IsSelected)
            {
                var selectionColor = Color.FromHex("#0288D1");
                BackgroundColor = Device.OS == TargetPlatform.Android
                    ? selectionColor
                    : selectionColor.AddLuminosity(0.05);
                Opacity = 1.0;
            }
            else
            {
                BackgroundColor = Color.Transparent;
                Opacity = 0.5;
            }
        }
    }
}