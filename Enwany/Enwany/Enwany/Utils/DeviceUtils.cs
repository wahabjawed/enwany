﻿using Xamarin.Forms;

namespace Enwany
{
    public class DeviceUtils
    {
        private static int ScreenWidth;
        private static int ScreenHeight;
        private static string OS;
        private static string DeviceId;

        public static bool getBoolean(string key)
        {
            return DependencyService.Get<ISharedPreference>().GetBoolean(key);
        }

        public static void setBoolean(string key, bool value)
        {
            DependencyService.Get<ISharedPreference>().SetBoolean(key, value);
        }

        public static void SetString(string key, string value)
        {
            DependencyService.Get<ISharedPreference>().SetString(key, value);
        }

        public static int GetScreenHeight()
        {
            return ScreenHeight;
        }

        public static string GetString(string key)
        {
            return DependencyService.Get<ISharedPreference>().GetString(key);
        }

        public static void SetScreenHeight(int screenHeight)
        {
            ScreenHeight = screenHeight;
        }

        public static int GetScreenWidth()
        {
            return ScreenWidth;
        }

        public static void SetScreenWidth(int screenWidth)
        {
            ScreenWidth = screenWidth;
        }

        public static string GetOS()
        {
            return OS;
        }

        public static void SetOS(string oS)
        {
            OS = oS;
        }

        public static string GetDeviceId()
        {
            return DeviceId;
        }

        public static void SetDeviceId(string deviceId)
        {
            DeviceId = deviceId;
        }
    }
}