﻿using Newtonsoft.Json.Linq;

namespace Enwany
{
    public class ParseUtils
    {
        public static string StringValue(JToken input, string key)
        {
            if (input == null || input[key] == null)
                return "";

            return input[key].ToString();
        }

        public static int IntValue(JToken input, string key)
        {
            var parseValue = 0;
            int.TryParse(StringValue(input, key), out parseValue);
            return parseValue;
        }

        public static decimal DecimalValue(JToken input, string key)
        {
            return decimal.Parse(StringValue(input, key));
        }

        public static double DoubleValue(JToken input, string key)
        {
            double returnValue = 0;

            double.TryParse(StringValue(input, key), out returnValue);

            return returnValue;
        }

        public static bool BoolValue(JToken input, string key)
        {
            return bool.Parse(StringValue(input, key));
        }
    }
}