﻿namespace Enwany
{
    public enum StyleType
    {
        Regular,
        Bold,
        Italic,
        BoldItalic
    }

    public class FontUtils
    {
        public const string FontFamily = "AvenirNextLTPro";

        public static string getFontOfType(StyleType Type)
        {
            var postfix = Type.ToString();
            return string.Format("{0}-{1}.ttf", FontFamily, postfix);
        }

        public static string getFontForiOSOfType(StyleType Type)
        {
            var postfix = Type.ToString();
            return string.Format("{0}-{1}", FontFamily, postfix);
        }

        public static string getFontOfTypeAndFamily(StyleType Type, string Family)
        {
            var postfix = Type.ToString();
            return string.Format("{0}-{1}.ttf", Family, postfix);
        }

        public static string getFontForiOSOfTypeAndFamily(StyleType Type, string Family)
        {
            var postfix = Type.ToString();
            return string.Format("{0}-{1}", Family, postfix);
        }
    }
}