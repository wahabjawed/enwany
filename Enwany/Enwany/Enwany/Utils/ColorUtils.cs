﻿using Xamarin.Forms;

namespace Enwany
{
    public class ColorUtils
    {
        public const string DarkBlue = "#0375a8"; //Blue font color

        public const string BackgroundDarkBlue = "#0074aa"; //Blue Background for service provider pin

        public const string Black = "#000000";
        public const string DarkGray = "#333333"; //Default text color
        public const string Gray = "#666666"; //Gray text color
        public const string White = "#ffffff";

        public const string GrayUnderline = "#787a7c"; // TPCustomEntry underline
        public const string LightGray = "#dedede"; // Seperator color
        public const string AppBackgroundColor = "#f0f4f7";

        public const string LightPink = "#f6f5f5";
        public const string GrayBackground = "#eef1f2";

        //label hightlight colors
        public const string RedHighlight = "#ffc7c9";
        public const string YellowHighlight = "#f8ecd0";
        public const string GreenHighlight = "#bde8d2";

        public const string Yellow = "#f2c144";
        public const string Red = "#ed1c24";
        public const string Green = "#00a651";

        public const string YellowBlind = "#ffd15c";
        public const string RedBlind = "#be171e";
        public const string GreenBlind = "#59c31f";


        public const string WhiteSmoke = "#f1f0f0";
        public const string SearchFilterBule = "#006fa3";

        public static Color ToColor(string colorCode)
        {
            return Color.FromHex(colorCode);
        }

        public static Color GetColorFromString(string colorName)
        {
            return Color.FromHex(colorName);
        }
    }
}