﻿using System.Collections.Generic;
using Xamarin.Forms;

namespace Enwany
{
    public abstract class Theme
    {
        public Theme()
        {
            TabBarSelectedColor = "#0388c3";
            TextColor = "#FFF";
            IconDashbaord = "icon_dashboard.png";
            IconSetting = "icon_settings.png";
            IconHelp = "icon_help.png";
            IconProfile = "icon_profile.png";
            IconUsername = "icon_username_volvo";
            IconPassword = "icon_password_volvo";
            IconGlobe = "icon_globe.png";
            IconGlobeSmall = "icon_globe_small.png";
            IconFilter = "icon_filter.png";
            IconFilterSmall = "icon_filter_small.png";
            IconMenu = "menu_icon.png";
            IconHome = "home_icon.png";
            IconList = "list_icon.png";
            EstimateApproveButtonColor = Green;
            EstimateDeclineButtonColor = Red;

            DashboardTabBarUnderlineHorizontalOption = LayoutOptions.CenterAndExpand;
        }

        public static string UptimeCenter { get; set; }

        public static ColorBand GetColor
        {
            get { return colorsPair[ColorBlindIndex]; }
        }

        public static string Yellow
        {
            get { return colorsPair[ColorBlindIndex].Yellow; }
        }

        public static string Red
        {
            get { return colorsPair[ColorBlindIndex].Red; }
        }

        public static string Green
        {
            get { return colorsPair[ColorBlindIndex].Green; }
        }

        public static Dictionary<bool, ColorBand> colorsPair
        {
            get
            {
                var colorsPair = new Dictionary<bool, ColorBand>();

                colorsPair.Add(false, new ColorBand(ColorUtils.Red, ColorUtils.Yellow, ColorUtils.Green));
                colorsPair.Add(true, new ColorBand(ColorUtils.RedBlind, ColorUtils.YellowBlind, ColorUtils.GreenBlind));

                return colorsPair;
            }
        }

        public static bool ColorBlindIndex { get; set; }

        public static string BaseColorDark { get; set; }

        public static string BaseColorMedium { get; set; }

        public static string BaseColorLight { get; set; }

        public static string SecondaryColorLight { get; set; }

        public static string SecondaryColorDark { get; set; }

        public static string NavBarItemSelected { get; set; }

        public static string TextColor { get; set; }

        public static string IconProfile { get; set; }

        public static string IconDashbaord { get; set; }

        public static string IconSetting { get; set; }

        public static string IconHelp { get; set; }

        public static string IconList { get; set; }

        public static string IconUsername { get; set; }

        public static string IconPassword { get; set; }

        public static string AppIcon { get; set; }

        public static string ThemeTitle { get; set; }

        public static string NavigationAppIcon { get; set; }

        public static string AboutAndHelpAppIcon { get; set; }

        public static string IconFilter { get; set; }

        public static string IconFilterSmall { get; set; }

        public static string IconGlobe { get; set; }

        public static string IconGlobeSmall { get; set; }

        public static string IconMenu { get; set; }

        public static string IconHome { get; set; }

        public static string NavBarTopColor { get; set; }

        public static string ActionBarLineColor { get; set; }

        public static string ProgressBarColor { get; set; }

        public static string DashboardTabBarColor { get; set; }
        public static string DashboardTabBarTextColor { get; set; }
        public static LayoutOptions DashboardTabBarUnderlineHorizontalOption { get; set; }
        public static string DashboardTabBarUnderlineDeselected { get; set; }

        public static string TabBarSelectedColor { get; set; }

        public static string EstimateApproveButtonColor { get; set; }
        public static string EstimateDeclineButtonColor { get; set; }
    }

    public class ColorBand
    {
        public Color[] colors;

        public Dictionary<string, string> colorsKVPair;

        public ColorBand(string red, string yellow, string green)
        {
            Red = red;
            Yellow = yellow;
            Green = green;
            colors = new Color[4]
            {Color.FromHex(Green), Color.FromHex(Red), Color.FromHex(Yellow), Color.FromHex(Green)};

            colorsKVPair = new Dictionary<string, string>
            {
                {"red", Red},
                {"green", Green},
                {"yellow", Yellow}
            };
        }

        public string Yellow { get; set; }

        public string Red { get; set; }

        public string Green { get; set; }
    }
}