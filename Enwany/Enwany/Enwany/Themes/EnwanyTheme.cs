﻿using Xamarin.Forms;

namespace Enwany
{
    public class EnwanyTheme : Theme
    {
        public EnwanyTheme()
        {
            SecondaryColorDark = "#333333";
            SecondaryColorLight = "#0375A8";

            BaseColorDark = "#0375a8";
            BaseColorMedium = "#0492d2";
            BaseColorLight = "#0388c3";

            NavBarItemSelected = "#0492d2";

            AppIcon = "app_icon_decisiv.png";

            NavigationAppIcon = "icon_decisiv.png";
            AboutAndHelpAppIcon = "icon_large_decisiv.png";
            ThemeTitle = "Enwany";

            ActionBarLineColor = BaseColorDark;

            ProgressBarColor = BaseColorMedium;


            Device.OnPlatform(
                () =>
                {
                    NavBarTopColor = BaseColorLight;
                    DashboardTabBarColor = "#ffffff";
                    DashboardTabBarTextColor = ColorUtils.Gray;
                    DashboardTabBarUnderlineHorizontalOption = LayoutOptions.Center;
                    DashboardTabBarUnderlineDeselected = "#00ffffff";
                },
                () =>
                {
                    NavBarTopColor = BaseColorMedium;
                    DashboardTabBarColor = BaseColorMedium;
                    DashboardTabBarTextColor = TextColor;
                    DashboardTabBarUnderlineHorizontalOption = LayoutOptions.FillAndExpand;
                    DashboardTabBarUnderlineDeselected = BaseColorLight;
                }
                );
        }
    }
}