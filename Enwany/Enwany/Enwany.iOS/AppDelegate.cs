﻿using FFImageLoading.Forms.Touch;
using Foundation;
using Google.MobileAds;
using UIKit;
using Xamarin;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

namespace Enwany.iOS
{
    // The UIApplicationDelegate for the application. This class is responsible for launching the 
    // User Interface of the application, as well as listening (and optionally responding) to 
    // application events from iOS.
    [Register("AppDelegate")]
    public class AppDelegate : FormsApplicationDelegate
    {
        //
        // This method is invoked when the application has loaded and is ready to run. In this 
        // method you should instantiate the window, load the UI into it and then make the window
        // visible.
        //
        // You have 17 seconds to return from this method, or iOS will terminate your application.
        //
        public override bool FinishedLaunching(UIApplication app, NSDictionary options)
        {
            UINavigationBar.Appearance.TintColor = UIColor.FromRGB(3, 169, 244);
			CachedImageRenderer.Init();
            Forms.Init();
			//Firebase.Analytics.App.Configure();
			FormsGoogleMaps.Init("AIzaSyCBcisUzWicmPMkuR8XkQ8J-VQAsOqHG4A");
			MobileAds.Configure("ca-app-pub-2447389984898739~2596082802");
			LoadApplication(new App());
			return base.FinishedLaunching(app, options);
        }
    }
}