﻿using Enwany.iOS;
using Foundation;
using Xamarin.Forms;

[assembly: Dependency(typeof(SharedPreference))]

namespace Enwany.iOS
{
    public class SharedPreference : ISharedPreference
    {
        public bool GetBoolean(string key)
        {
            var value = NSUserDefaults.StandardUserDefaults.BoolForKey(key);
            return value;
        }

        public void SetBoolean(string key, bool value)
        {
            NSUserDefaults.StandardUserDefaults.SetBool(value, key);
            NSUserDefaults.StandardUserDefaults.Synchronize();
        }

        public void SetString(string key, string value)
        {
            NSUserDefaults.StandardUserDefaults.SetString(value, key);
            NSUserDefaults.StandardUserDefaults.Synchronize();
        }

        public string GetString(string key)
        {
            var value = NSUserDefaults.StandardUserDefaults.StringForKey(key);
            if (value == null)
                return "";
            return value;
        }
    }
}