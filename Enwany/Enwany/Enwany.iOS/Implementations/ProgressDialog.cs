﻿using BigTed;
using Enwany.iOS;
using Xamarin.Forms;

[assembly: Dependency(typeof(ProgressDialog))]

namespace Enwany.iOS
{
    public class ProgressDialog : IProgressDialog
    {
        public void Show()
        {
            BTProgressHUD.Show(maskType: ProgressHUD.MaskType.Black);
        }

        public void Show(string message)
        {
            BTProgressHUD.Show(message);
        }

        public void Dismiss()
        {
            BTProgressHUD.Dismiss();
        }
    }
}