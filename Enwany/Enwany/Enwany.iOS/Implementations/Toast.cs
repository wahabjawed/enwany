﻿using BigTed;
using Enwany.iOS;
using Xamarin.Forms;

[assembly: Dependency(typeof(Toast))]

namespace Enwany.iOS
{
    public class Toast : IToast
    {
        public void Show(string message)
        {
            BTProgressHUD.ShowToast(message, ProgressHUD.MaskType.Black, true, 2250);
        }
    }
}