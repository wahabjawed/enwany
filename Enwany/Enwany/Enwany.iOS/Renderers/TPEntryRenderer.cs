﻿using Enwany;
using Enwany.iOS;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(TPEntry), typeof(TPEntryRenderer))]

namespace Enwany.iOS
{
    public class TPEntryRenderer : EntryRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);
            if (Control == null)
                return;

            Control.BorderStyle = UITextBorderStyle.None;
            Control.ClearButtonMode = UITextFieldViewMode.WhileEditing;
        }
    }
}