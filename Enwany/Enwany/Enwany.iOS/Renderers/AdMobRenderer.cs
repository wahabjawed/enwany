using Enwany;
using Enwany.iOS;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using Google.MobileAds;
using System.Drawing;
using CoreGraphics;

[assembly: ExportRenderer(typeof(AdMobView), typeof(AdMobRenderer))]

namespace Enwany.iOS
{
    public class AdMobRenderer : ViewRenderer
    {
		private const string AdmobID = "ca-app-pub-2447389984898739/5604118001";
			//"ca-app-pub-3940256099942544/6300978111";
        BannerView adView;
        bool viewOnScreen;

        protected override void OnElementChanged(ElementChangedEventArgs<View> e)
        {
            base.OnElementChanged(e);

            if (e.NewElement == null)
                return;

            if (e.OldElement == null)
            {
				UIViewController viewCtrl = null;

				foreach (UIWindow v in UIApplication.SharedApplication.Windows)
				{
					if (v.RootViewController != null)
					{
						viewCtrl = v.RootViewController;
					}
				}


				adView = new BannerView(size: AdSizeCons.Banner, origin: new CGPoint(-10, 0) )
			   {
				   AdUnitID = AdmobID,
				   RootViewController = viewCtrl
			   };

                adView.AdReceived += (sender, args) =>
                {
                    if (!viewOnScreen) this.AddSubview(adView);
                    viewOnScreen = true;
                };

                adView.LoadRequest(Request.GetDefaultRequest());
                base.SetNativeControl(adView);
            }
        }
    }
}