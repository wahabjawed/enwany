﻿using System;
using Enwany;
using Enwany.iOS;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(TPButton), typeof(TPButtonRenderer))]

namespace Enwany.iOS
{
    public class TPButtonRenderer : ButtonRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Button> e)
        {
            base.OnElementChanged(e);

            var button = (TPButton) Element;
            if (button == null) return;

            var style = button.FontStyle;
            var fontFamily = button.FontFamily;
            var font = fontFamily == null
                ? FontUtils.getFontForiOSOfType(style)
                : FontUtils.getFontForiOSOfTypeAndFamily(style, fontFamily);
            var bgcolor = button.BackgroundColor;
            var textcolor = button.TextColor;

            var nativeButton = Control;
            if (nativeButton == null)
                return;

            //button.nativeButtonClicker += pressNativeButton;

            nativeButton.Font = UIFont.FromName(font, Control.Font.PointSize);
            nativeButton.BackgroundColor = bgcolor.ToUIColor();
            nativeButton.SetTitleColor(textcolor.ToUIColor(), UIControlState.Normal);

            if (!button.BackgroundColor.Equals(Color.Transparent))
            {
                nativeButton.ContentEdgeInsets = new UIEdgeInsets(0, 10, 0, 10);
            }
        }

        private void pressNativeButton(object sender, EventArgs e)
        {
            Control.SendActionForControlEvents(UIControlEvent.TouchUpInside);
        }
    }
}