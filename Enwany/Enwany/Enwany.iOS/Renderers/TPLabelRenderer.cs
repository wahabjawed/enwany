﻿using System.ComponentModel;
using Enwany;
using Enwany.iOS;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(TPLabel), typeof(TPLabelRenderer))]

namespace Enwany.iOS
{
    public class TPLabelRenderer : LabelRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Label> e)
        {
            base.OnElementChanged(e);
            setFont();
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);
            setFont();
        }

        private void setFont()
        {
            if (Control == null) return;

            var label = (TPLabel) Element;
            if (label == null) return;

            var style = label.FontStyle;

            var fontFamily = label.FontFamily;

            var font = fontFamily == null
                ? FontUtils.getFontForiOSOfType(style)
                : FontUtils.getFontForiOSOfTypeAndFamily(style, fontFamily);
            var nativeLabel = Control;

            nativeLabel.Font = UIFont.FromName(font, Control.Font.PointSize);
        }
    }
}