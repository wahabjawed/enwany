﻿using System.ComponentModel;
using Enwany;
using Enwany.iOS;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(TPCheckBox), typeof(TPCheckBoxRenderer))]

namespace Enwany.iOS
{
    public class TPCheckBoxRenderer : ViewRenderer<TPCheckBox, UIButton>
    {
        protected override void OnElementChanged(ElementChangedEventArgs<TPCheckBox> e)
        {
            base.OnElementChanged(e);

            if (Element == null) return;

            BackgroundColor = Element.BackgroundColor.ToUIColor();
            if (e.NewElement != null)
            {
                if (Control == null)
                {
                    var checkBox = new UIButton(Bounds);
                    SetNativeControl(checkBox);
                    setCheckboxImg(e.NewElement.Checked);
                }
                Control.LineBreakMode = UILineBreakMode.CharacterWrap;
                Control.VerticalAlignment = UIControlContentVerticalAlignment.Top;
                Control.AdjustsImageWhenDisabled = false;
            }

            Control.Frame = Frame;
            Control.Bounds = Bounds;
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);
            if (e.PropertyName.Equals("Checked"))
            {
                setCheckboxImg(Element.Checked);
            }
        }

        private void setCheckboxImg(bool isChecked)
        {
            if (isChecked)
                Control.SetImage(UIImage.FromBundle("check"), UIControlState.Normal);
            else
                Control.SetImage(UIImage.FromBundle("uncheck"), UIControlState.Normal);
        }
    }
}